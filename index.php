<!DOCTYPE html>
<html>
<head><title>Test</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Angular library -->
<script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<style>
div.container {
	width:98%;
	vertical-align:text-top;
}
div.control {
	margin:auto;
	padding:20px;
}

table {
	width: 100%;
    border-collapse: collapse;
}

table, th, td {
    border: 1px solid black;
}
</style>
</head>
<body>
<div ng-app="test" ng-controller="testCtrl" ng-init="A=150;B=150;C=100;D=100;E=200;n=200" class="container">
	<div class="control" >
		A:<input ng-model="A" type="text">
		B:<input ng-model="B" type="text">
		C:<input ng-model="C" type="text">
		D:<input ng-model="D" type="text">
		E:<input ng-model="E" type="text">
		Num of Orders:<input ng-model="n" type="text">
		<button ng-click="start()">GO</button>
	</div>
	<br/><br/>
	<table>
		<tr>
			<td>Return Code: {{ data.code }} Message: {{ data.message }}</td>
		</tr>
		<tr>
			<th>Orders</th>
			<th>Allocation</th>
		</tr>
		<tr>
			<td width=75% valign=top>
			<div ng-repeat="x in data.orders">{{ x }}</div>
			</td>
			<td width=15% valign=top>
			<div ng-repeat="y in data.allocation">{{ y }}</div>
			</td>
		</tr>
	</table>
</div>
<script>
var app= angular.module('test', []);

app.controller('testCtrl', function($scope, $http) {
    $scope.start = function() {
    	console.log('click'+$scope.A);
    	$http({
    		method:"GET",
    		url: "test.php",
    		params: {
    			'a':$scope.A,
    			'b':$scope.B,
    			'c':$scope.C,
    			'd':$scope.D,
    			'e':$scope.E,
    			'n':$scope.n
    			}
    	})
	    .then(function(response) {
        	$scope.data = response.data;
	    });
    }
});

</script>
</body>
</html>
