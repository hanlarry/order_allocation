<?php

include_once("InventoryAllocator.php");
include_once("Order.php");

$ia = InventoryAllocator::get_allocator();

//initialize
$ia->init($_REQUEST['a'],$_REQUEST['b'],$_REQUEST['c'],$_REQUEST['d'],$_REQUEST['e']);

$ret = array();

//generate 200 random orders
$ord = new Order();
$orders = $ord->generate_orders($_REQUEST['n']);
$ret['orders'] = $orders;

//send orders and process
foreach ($orders as $order) {
	$ia->receive($order);
}

while (true) {
	try {
		$ia->process();
	} catch (Exception $e) {
		if ($e->getMessage()=='No Inventory') {
			$ret['allocation'] = $ia->report();
			$ret['code'] = 1;
			$ret['message'] = $e->getMessage();
		} elseif ($e->getMessage()=='No Order') {
			$ret['allocation'] = $ia->report();
			$ret['code'] = 0;
			$ret['message'] = $e->getMessage();
		} else {
			$ret['code'] = -1;
			$ret['message'] = $e->getMessage();
		}
		break;
	}
}

header('Content-Type: application/json');
echo json_encode($ret);