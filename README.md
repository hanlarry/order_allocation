# Synopsis #

This a small project to simulate inventory allocation process.

### Source file ###

* InventoryAllocator.php
* Order.php
* test.php
* index.php

### Installation ###

Clone the repository into a directory under a web server document root directory. 

### Usage ###

1, Get InventoryAllocator instance
```
$ia = InventoryAllocator::get_allocator();
```
2, initialize inventory
```
$ia->init(150,150,100,100,200);
```
3, generate n(for example 200) randow orders
```
$ord = new Order();
$orders = $ord->generate_orders(200);
```
4, receive orders 
```
foreach ($orders as $order) {
	$ia->receive($order);
}
```
5, process orders
```
$ia->process();
```
### Test ###

Open a browser and point to the cloned directory.  The index page will show up with simple interface to set parameters and test.

