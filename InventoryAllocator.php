<?php

/**
 *	Inventory Allocator
 *	Allocates inventory to the inbound orders
 *
 *	@author: Larry Han
 *
 */
class InventoryAllocator 
{

	const MIN_QUANTITY = 1;
	const MAX_QUANTITY = 5;

	private $_inventory = array();
	private $_order_queue = array();
	private $_allocation = array();
	private static $_inventory_allocator = null;

	private function __construct() {}

	/**
	 * get a singlton instance
	 * @param:void
	 * @return: object
	 */
	static public function get_allocator() 
	{
		if (self::$_inventory_allocator===null) {
			self::$_inventory_allocator = new InventoryAllocator();
		}
		return self::$_inventory_allocator;
	}

	/**
	 *	assign initial values
	 *	@param: int
	 *	@return: void
	 */
	function init($a=0,$b=0,$c=0,$d=0,$e=0) 
	{
		$this->_inventory['A'] = $a;
		$this->_inventory['B'] = $b;
		$this->_inventory['C'] = $c;
		$this->_inventory['D'] = $d;
		$this->_inventory['E'] = $e;
	}

	/**
	 * put new order in queue
	 * @param: array
	 * @return: void
	 *
	 */
	function  receive($order) 
	{
		if (!empty($order)) {
			array_unshift($this->_order_queue, $order);
		}
	}

	/**
	 * pop an order from queue
	 * @param: void
	 * @return: array
	 *
	 */
	function dequeue() 
	{
		if (!empty($this->_order_queue)) {
			return array_pop($this->_order_queue);
		} else {
			throw new Exception("No Order");
		}
	}


	/**
	 * check if there is any inventory
	 * @param: void
	 * @return: boolean
	 *
	 */
	 function no_inventory() 
	 {
	 	foreach ($this->_inventory as $key=>$val) {
	 		if ($val > 0) {
	 			return false;
	 		}
	 	}
	 	return true;
	 }

	 /**
	  * process orders
	  * @param: void
	  * @return: void
	  */
	 function process() 
	 {
	 	$order_hash = array();
	 	if ($this->no_inventory()) {
	 		throw new Exception("No Inventory");
	 	} else {
	 		$order = json_decode($this->dequeue());

	 		//hash each order, assume two orders with same hash are the same order
	 		$hash = md5($order);
	 		if (array_key_exists($hash, $order_hash)) {
	 			continue;
	 		} else {
	 			$order_hash[$hash] = 1;
	 			$allocation = $this->allocate($order);
	 			$this->_allocation[] = $allocation;
	 		}
	 	}
	 }

	 /**
	  *	allocate one order
	  * @param: array
	  * @return: array
	  */
	 function allocate($order) 
	 {
	 	$header = $order->Header;
	 	$allocation = array(
	 		'header' => $header,
	 		'order' => array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0),
	 		'fufill'=> array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0),
	 		'backordered' => array('A'=>0,'B'=>0,'C'=>0,'D'=>0,'E'=>0)
	 	);

		$lines = $order->Lines;
		if (!is_array($lines)) {
			$lines = array($lines);
		}
	 	foreach ($lines as $line) {
			$product = $line->Product;
			$quantity = $line->Quantity;
			if ($quantity < self::MIN_QUANTITY || $quantity > self::MAX_QUANTITY) {
				return false;
			}
			if ($this->_inventory[$product] < $quantity) {
				// todo: is partial allocation valid?
				$fufill = 0;
				$backordered = $quantity;
			} else {
				$fufill = $quantity;
				$backordered = 0;
				$this->_inventory[$product] -= $quantity;
			}
			$allocation['fufill'][$product]=$fufill;
			$allocation['order'][$product]=$quantity;
			$allocation['backordered'][$product]=$backordered;
		};
		return $allocation;
	 }

	  /**
	   * final report
	   * @param:void
	   * @return: void
	   * output format:
	   * 	1: 1,0,1,0,0::1,0,1,0,0::0,0,0,0,0
	   *	2: 0,0,0,0,5::0,0,0,0,0::0,0,0,0,5
	   *	3: 0,0,0,4,0::0,0,0,0,0::0,0,0,4,0
	   *	4: 1,0,1,0,0::1,0,0,0,0::0,0,1,0,0
	   *	5: 0,3,0,0,0::0,3,0,0,0::0,0,0,0,0
	   */
	 function report() 
	 {
	 	$res = array();
		foreach($this->_allocation as $output) {
			$row = sprintf("%s:%s.%s,%s.%s,%s::%s.%s,%s.%s,%s::%s.%s,%s.%s,%s",
				$output['header'],
				$output['order']['A'],$output['order']['B'],$output['order']['C'],$output['order']['D'],$output['order']['E'],
				$output['fufill']['A'],$output['fufill']['B'],$output['fufill']['C'],$output['fufill']['D'],$output['fufill']['E'],
				$output['backordered']['A'],$output['backordered']['B'],$output['backordered']['C'],$output['backordered']['D'],$output['backordered']['E']);
			$res[] = $row;
		};
		return $res;
	 }
}