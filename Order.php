<?php

/**
 * Order
 * Generating one or more streams of orders
 *
 * @auther: Larry Han
 *
 */

class Order 
{

	const MIN_QUANTITY = 1;
	const MAX_QUANTITY = 5;

	public $product = array('A','B','C','D','E');

	private $_order_queue = array();

	/**
	 * generate an random order
	 * @param: void
	 * @return: array
	 */
	function gen_order() 
	{
		$order = array();
		foreach ($this->product as $p) {
			if (rand(0,1)==1) {
				$quantity = $this->gen_quantity();
				$order[] = array('Product'=>$p, 'Quantity'=>$quantity);
			}
		}
		return $order;
	}

	/**
	 * generate random quantity between 1 ot 5
	 * @param: void
	 * @return: integer
	 */
	function gen_quantity() 
	{
		return rand(self::MIN_QUANTITY, self::MAX_QUANTITY);
	}

	/**
	 * generate orders
	 * @param: int
	 * @return: array
	 */
	function generate_orders($n) 
	{
		$i=1;
		while ($i<=$n) {
			$order = $this->gen_order();
			if (empty($order)) {
				continue;
			}
			$this->_order_queue[] = json_encode(array('Header'=>$i, 'Lines'=>$order));
			$i++;
		}
		return $this->_order_queue;
	}
}
